export const commonConstants = {
  filterConstant: {
    colorFilters: [
      {
        key: "Red",
        value: false,
      },
      {
        key: "Blue",
        value: false,
      },
      {
        key: "Green",
        value: false,
      },
    ],
    genderFilter: [
      {
        key: "Men",
        value: false,
      },
      {
        key: "Women",
        value: false,
      },
    ],
    priceFilters: [
      {
        key: "0-250",
        value: false,
      },
      {
        key: "250-450",
        value: false,
      },
      {
        key: "450-1000",
        value: false,
      },
    ],
    typeFilters: [
      {
        key: "Polo",
        value: false,
      },
      {
        key: "Hoodie",
        value: false,
      },
      {
        key: "Basic",
        value: false,
      },
    ],
  },
};
