import React from "react";
import { useHistory } from "react-router-dom";
import { Box } from "@mui/system";
import { Button } from "@mui/material";
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import "./Header.css";

const Header = ({itemsCount}) => {
  const history = useHistory();
  const handleCart =()=>{
    history.push("/checkout");
  }
  const handleHome =()=>{
    history.push("/");
  }
  return (
    <Box className="header-wrapper">
      <Box className="header-title">
        <Button variant="contained" color="primary" onClick={handleHome}>Terex Store</Button>
      </Box>
      <Box className="header-end">
        <Button variant="text" id="header-products" onClick={handleHome} >Products</Button>
        <Button onClick={handleCart} ><ShoppingCartOutlinedIcon/><Box mt="-15px" variant="sub">{itemsCount}</Box></Button>        
      </Box>
    </Box>
  );
};

export default Header;
