import React from "react";
import { Box } from "@mui/system";
import FormLabel from "@mui/material/FormLabel";
import FormControl from "@mui/material/FormControl";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";

const FilterTab = (props) => {
  const {colors, gender, price, type, handleChange} = props;
  return (
    <Box sx={{ marginTop: "25px" }}>
      <FilterForm
        option="Colours"
        items={colors}
        handleFunction={handleChange}
      ></FilterForm>
      <FilterForm
        option="Gender"
        items={gender}
        handleFunction={handleChange}
      ></FilterForm>
      <FilterForm
        option="Price"
        items={price}
        handleFunction={handleChange}
      ></FilterForm>
      <FilterForm
        option="Type"
        items={type}
        handleFunction={handleChange}
      ></FilterForm>
    </Box>
  );
};

const FilterForm = ({ option, items, handleFunction }) => {
  return (
    <Box sx={{ background: "#f4f4f3e3", marginBottom: "10px" }}>
      <FormControl sx={{ m: 3 }} component="fieldset" variant="standard">
        <FormLabel component="legend">{option}</FormLabel>
        <FormGroup row>
          {items.map((item, index) => (
            <FormControlLabel
              key={index}
              control={
                <Checkbox
                  checked={item.value}
                  onChange={() => handleFunction(option, item.key)}
                  name={item.key}
                />
              }
              label={option === "Price" ? `Rs.${item.key}` : item.key }
            />
          ))}
        </FormGroup>
      </FormControl>
    </Box>
  );
};

export default FilterTab;
