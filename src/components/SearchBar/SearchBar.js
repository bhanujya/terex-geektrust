import React,{useState} from 'react'
import { TextField } from "@mui/material";
import { InputAdornment } from "@mui/material";
import { Search } from "@mui/icons-material";
import { Box } from "@mui/system";

const SearchBar = ({ children, handleSearch }) => {

  const [debounceTimeout, setDebounceTimeout] = useState(500);
  const debounceSearch = (event, debounceTimeout) => {
    const value = event.target.value;
    if (debounceTimeout) {
      clearTimeout(debounceTimeout);
    }
    const timeout = setTimeout(() => {
      handleSearch(value)
    }, 500);
    setDebounceTimeout(timeout);
  };
  return (
    <Box display="flex" justifyContent="center">
      <Box>
        <TextField
          size="small"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Search color="primary" />
              </InputAdornment>
            ),
          }}
          placeholder="Search for items/categories"
          name="search"
          onChange={(e) => debounceSearch(e, debounceTimeout)}
        />
      </Box>
      <Box>{children}</Box>
    </Box>
  );
};
export default SearchBar;
