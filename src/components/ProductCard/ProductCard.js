import React from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  Grid,
  CardActionArea,
} from "@mui/material";
import { AddShoppingCartOutlined } from "@mui/icons-material";

const ProductCard = ({ data, handleAddToCart }) => {
  return (
    <Grid
      container
      spacing={2}
      columns={12}
      sx={{
        marginTop: "25px",
        background: "#f4f4f3e3",
        paddingRight: "16px",
        marginLeft: "-8px",
      }}
    >
      {data.length > 0 &&
        data.map((cart) => (
          <Grid item xs={12} sm={6} md={6} lg={4} key={cart.id}>
            <Card className="card">
              <CardContent>
                <Typography color="primary">{cart.name}</Typography>
              </CardContent>
              <CardActionArea>
                <CardMedia
                  component="img"
                  image={cart.imageURL}
                  sx={{ height: "10rem", objectFit: "contain" }}
                />
              </CardActionArea>
              <CardActions
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  flexWrap: "wrap",
                }}
              >
                <CardContent>
                  <Typography color="primary">Rs {cart.price}</Typography>
                </CardContent>
                <Button
                  className="card-button"
                  variant="contained"
                  onClick={() => handleAddToCart(cart)}
                  display="flex"
                  justifyContent="center"
                >
                  <AddShoppingCartOutlined />
                  add to cart
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
    </Grid>
  );
};

export default ProductCard;
