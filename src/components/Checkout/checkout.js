import React, { useEffect, useState } from "react";
import Header from "../Header/Header";
import { Box } from "@mui/system";
import "./checkout.css";
import {
  AddOutlined,
  RemoveOutlined,
  ShoppingCartOutlined,
} from "@mui/icons-material";
import { Grid, CardMedia, IconButton, Stack } from "@mui/material";
import { useSnackbar } from "notistack";

const countCartItems = (items) => {
  let count = 0;
  if (items && items.length > 0) {
    items.forEach((item) => {
      count += item.qty;
    });
  }
  return count;
};
const Checkout = () => {
  const { enqueueSnackbar } = useSnackbar();
  const items = JSON.parse(localStorage.getItem("itemsInCart"));
  const [itemsInCart, updateItemInCart] = useState(items);
  const [countOfItemsInCart, setCountOfItemsInCart] = useState();
  useEffect(() => {
    localStorage.setItem("itemsInCart", JSON.stringify(itemsInCart));
    setCountOfItemsInCart(countCartItems(itemsInCart));
  }, [itemsInCart]);

  const handleDelete = (selected) => {
    const updatedCart = itemsInCart
      .map((item) => {
        return item.id === selected.id
          ? {
              ...item,
              qty: selected.qty - 1,
            }
          : item;
      })
      .filter((item) => {
        return item.qty > 0;
      });
    updateItemInCart(updatedCart);
  };
  const handleAdd = (selected) => {
    const updatedCart = itemsInCart.map((item) => {
      if (item.id === selected.id && selected.quantity >= selected.qty + 1) {
        return {
          ...item,
          qty: selected.qty + 1,
        };
      } else if (
        item.id === selected.id &&
        selected.quantity < selected.qty + 1
      ) {
        enqueueSnackbar(`Max Quantity available for ${selected.name} is already added to Cart`, {
          variant: "info",
        });
        return { ...item };
      } else {
        return { ...item };
      }
    });
    updateItemInCart(updatedCart);
  };
  const ItemQuantity = ({ selected }) => {
    return (
      <>
        <Stack direction="row" alignItems="center">
          <>
            <IconButton
              size="small"
              color="primary"
              onClick={() => handleDelete(selected)}
            >
              <RemoveOutlined />
            </IconButton>
            <Box padding="0.5rem" data-testid="item-qty">
              {selected.qty}
            </Box>
            <IconButton
              size="small"
              color="primary"
              onClick={() => handleAdd(selected)}
            >
              <AddOutlined />
            </IconButton>
          </>
        </Stack>
      </>
    );
  };

  const totalCost = (items) => {
    let totalSum = 0;
    itemsInCart.forEach((item) => {
      totalSum += item.price * item.qty;
    });
    return totalSum;
  };
  return (
    <>
      <Header itemsCount={countOfItemsInCart}></Header>
      {countOfItemsInCart === 0 ? (
        <Box className="cart empty">
          <ShoppingCartOutlined className="empty-cart-icon" />
          <Box color="#aaa" textAlign="center">
            Cart is empty. Add more items to the cart to checkout.
          </Box>
        </Box>
      ) : (
        <Box>
          {itemsInCart.map((item) => (
            <Grid
              container
              justify="space-around"
              spacing={2}
              columns={12}
              key={item.id}
              sx={{
                background: "#f4f4f3e3",
                margin: "1rem 1rem 1rem 0",
                width: "100%",
              }}
            >
              <Grid item xs={12} sm={4} md={3}>
                <CardMedia
                  component="img"
                  image={item.imageURL}
                  sx={{ height: "10rem", objectFit: "contain" }}
                />
              </Grid>
              <Grid item xs={4} sm={3} md={2} className="grid-items">
                {item.name}
              </Grid>
              <Grid item xs={4} sm={3} md={2} className="grid-items">
                <ItemQuantity selected={item} />
              </Grid>
              <Grid item xs={4} sm={2} md={2} className="grid-items">
                Rs. {item.price * item.qty}
              </Grid>
            </Grid>
          ))}
          <Grid
            container
            spacing={2}
            columns={12}
            sx={{
              background: "#f4f4f3e3",
              margin: "1rem 1rem 1rem 0",
              width: "100%",
            }}
          >
            <Grid item xs={6} sm={10} md={7}>
              Order Total
            </Grid>
            <Grid item xs={6} sm={2} md={2}>
              Rs. {totalCost(items)}
            </Grid>
          </Grid>
        </Box>
      )}
    </>
  );
};

export default Checkout;
export { countCartItems };
