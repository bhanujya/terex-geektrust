import React, { useEffect, useState } from "react";
import Header from "../Header/Header";
import SearchBar from "../SearchBar/SearchBar";
import axios from "axios";
import { Grid } from "@mui/material";
import { CircularProgress } from "@mui/material";
import { Button, Stack } from "@mui/material";
import { config } from "../../App";
import FilterTab from "../FilterTab/FilterTab";
import ProductCard from "../ProductCard/ProductCard";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import { useSnackbar } from "notistack";
import {countCartItems} from "../Checkout/checkout";
import { commonConstants } from "../CommonConstants";

const Home = () => {
  const constants = commonConstants.filterConstant;
  const [showSpinner, setShowSpinner] = useState(true);
  const [cardItems, setCardItems] = useState();
  const [filterItems, setFilterItems] = useState();
  const [open, setOpen] = useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));

  const [colors, setColors] = useState(constants.colorFilters);
  const [gender, setGender] = useState(constants.genderFilter);
  const [price, setPrice] = useState(constants.priceFilters);
  const [type, setType] = useState(constants.typeFilters);

  const [filterByActive, setFilterByActive] = useState(false);
  const [searchValue, setSearchValue] = useState(undefined);

  const [colorFiltersArr, setColorFiltersArr] = useState([]);
  const [genderFiltersArr, setGenderFiltersArrr] = useState([]);
  const [priceFiltersArr, setPriceFiltersArrr] = useState([]);
  const [typeFilterArr, setTypeFilterArr] = useState([]);

  const itemsInLocalStrage = JSON.parse(localStorage.getItem("itemsInCart")) ? JSON.parse(localStorage.getItem("itemsInCart")) : [] ;
  const [itemsInCart, addItemInCart] = useState(itemsInLocalStrage);
  const [countOfItemsInCart, setCountOfItemsInCart] = useState();

  const { enqueueSnackbar } = useSnackbar();

  // function to open the filter options for mobile view
  const handleFilterMobileView = () => {
    setOpen(true);
    setCardItems(filterItems);
  };

  // function to close the filter options for mobile view
  const closeFilterMobileView = () => {
    setOpen(false);
  };

  useEffect(() => {
    fetchItems();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    updateProductList();
    // eslint-disable-next-line
  }, [
    colorFiltersArr,
    genderFiltersArr,
    typeFilterArr,
    priceFiltersArr,
    searchValue,
  ]);

  useEffect(()=>{    
    localStorage.setItem("itemsInCart",JSON.stringify(itemsInCart));
    setCountOfItemsInCart(countCartItems(itemsInCart));
  },[itemsInCart])

  // function to hit the endpoint and get the products list
  const fetchItems = async () => {
    const url = config.endpoint;
    try {
      const response = await axios.get(url);
      setCardItems(response.data);
      setFilterItems(response.data);
      setShowSpinner(false);
    } catch (e) {
      setShowSpinner(false);
      enqueueSnackbar(
        `Unable to fetch Products. Try again after some time`,
        { variant: "error" }
      );
    }
  };

  // function to update the Product Items on Filter and Search
  const updateProductList = () => {
    if (filterByActive || searchValue) {
      const filteredItems = filterItems
        .filter((item) => {
          return (
            colorFiltersArr.includes(item.color) ||
            (colorFiltersArr.length === 0 && item)
          );
        })
        .filter((item) => {
          return (
            genderFiltersArr.includes(item.gender) ||
            (genderFiltersArr.length === 0 && item)
          );
        })
        .filter((item) => {
          return (
            typeFilterArr.includes(item.type) ||
            (typeFilterArr.length === 0 && item)
          );
        })
        .filter((item) => {
          for (let i = 0; i < priceFiltersArr.length; i++) {
            const low = parseInt(priceFiltersArr[i].split("-")[0]);
            const high = parseInt(priceFiltersArr[i].split("-")[1]);
            if (item.price > low && item.price <= high) {
              return true;
            }
          }
          return priceFiltersArr.length === 0 ? true : false;
        })
        .filter((item) => {
          if (
            searchValue === undefined ||
            searchValue === "" ||
            item.name.toLowerCase() === searchValue.toLowerCase()
          ) {
            return true;
          }
          const searchParams = searchValue.split(" ");
          for (let i = 0; i < searchParams.length; i++) {
            const param = searchParams[i].toLowerCase();
            if (
              item.color.toLowerCase() === param ||
              item.type.toLowerCase() === param
            ) {
              return true;
            }
          }
          return false;
        });
      setCardItems(filteredItems);
    }
  };

  // update the filters[ colours, gender, type, price] obj use for check/uncheck in the UI
  const updateObj = (obj, key) => {
    const updatedObj = obj.map((item) => {
      if (item.key === key) {
        return { ...item, value: !item.value };
      }
      return { ...item };
    });
    return updatedObj;
  };

  // function to update the selected filters array eg. for colours ["Red","Blue"]
  const updateFiltersArr = (arr, setArr, key) => {
    if (arr.includes(key)) {
      setArr(arr.filter((item) => item !== key));
    } else {
      setArr((prev) => [...prev, key]);
    }
  };

  // function to handle the filter change. Called from the UI
  const handleFilterChange = (option, key) => {
    setFilterByActive(true);
    switch (option) {
      case "Colours":
        const updatedColor = updateObj(colors, key);
        setColors(updatedColor);
        updateFiltersArr(colorFiltersArr, setColorFiltersArr, key);
        break;
      case "Gender":
        const updatedGender = updateObj(gender, key);
        setGender(updatedGender);
        updateFiltersArr(genderFiltersArr, setGenderFiltersArrr, key);
        break;
      case "Price":
        const updatedPrice = updateObj(price, key);
        setPrice(updatedPrice);
        updateFiltersArr(priceFiltersArr, setPriceFiltersArrr, key);
        break;
      case "Type":
        const updatedType = updateObj(type, key);
        setType(updatedType);
        updateFiltersArr(typeFilterArr, setTypeFilterArr, key);
        break;
      default:
    }
  };

  // function called when Add to Cart is clicked
  const addToCart = (selected) => {
    // check if the item is in Cart.
    const isItemInCart = checkItemInCart(selected.id);
    if (isItemInCart) {
      enqueueSnackbar(
        `Item ${selected.name} already in Cart. Visit Cart to add/remove`,
        { variant: "info" }
      );
    } else {
      addItemInCart((prev) => [
        ...prev,
        {
          ...selected,
          qty: 1
        },
      ]);
      enqueueSnackbar(
        `Item ${selected.name} added to Cart. Visit Cart to increase the quantity`,
        { variant: "success" }
      );
    }
  };

  const checkItemInCart = (id) => {
    for (const item of itemsInCart) {
      if( item.id === id ) return true;
    }
    return false;
  };

  return (
    <>
      <Header itemsCount={countOfItemsInCart}></Header>
      <SearchBar handleSearch={(value) => setSearchValue(value)}>
        <Button
          sx={{ display: { xs: "block", sm: "none" } }}
          onClick={handleFilterMobileView}
        >
          <FilterAltIcon />
        </Button>
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={closeFilterMobileView}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle>
            <Grid container justify="space-between">
              <Grid item>Filters</Grid>
              <Grid item>
                <IconButton
                  aria-label="close"
                  onClick={closeFilterMobileView}
                  sx={{
                    position: "absolute",
                    right: 8,
                    top: 8,
                    color: "black",
                  }}
                >
                  <CloseIcon />
                </IconButton>
              </Grid>
            </Grid>
          </DialogTitle>
          <FilterTab
            handleChange={handleFilterChange}
            colors={colors}
            gender={gender}
            price={price}
            type={type}
          ></FilterTab>
        </Dialog>
      </SearchBar>

      {showSpinner ? (
        <Stack alignItems="center">
          <CircularProgress />
        </Stack>
      ) : (
        <Grid container justify="space-around" spacing={2} columns={12}>
          <Grid
            item
            sm={4}
            md={4}
            sx={{ display: { xs: "none", sm: "block" } }}
          >
            <FilterTab
              handleChange={handleFilterChange}
              colors={colors}
              gender={gender}
              price={price}
              type={type}
            ></FilterTab>
          </Grid>
          <Grid item xs={12} sm={8} md={8}>
            <ProductCard
              data={cardItems}
              handleAddToCart={addToCart}
            ></ProductCard>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default Home;
