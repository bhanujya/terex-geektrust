import { createTheme } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    primary: {
      light: "#45c09f",
      main: "#00a278",
      dark: "#00845c",
      contrastText: "#fff",
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 400,
      md: 750,
      lg: 900,
      xl: 1200,
      tablet: 1024,
    },
  },
});

export default theme;
