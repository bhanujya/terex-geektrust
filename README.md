# terex-geektrust
## Getting started

```
cd existing_repo
git remote add origin https://gitlab.com/bhanujya/terex-geektrust.git
git branch -M main
git push -uf origin main
```

## Name
Teerex-GeekTrust Frontend-challenge

## Description
This is a frontent challange from geekTrust. The task is to create a web e-commerce responsive website, where an user can add items to cart, seach an item, display specific items depending on the filters used.
The items added to cart are stored in the localStorage instead of using any database.

## steps to run in local
npm install
npm start